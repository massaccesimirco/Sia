- Add parsing of module names to `siad -M` and automatically enable the
    `accounting`and `feemanager` modules if the `wallet` is enabled.
